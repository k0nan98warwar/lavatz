﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public BulletConfig bullet; //ScriptableObject
    
    Rigidbody myRig;
    Vector3 target;
    /// <summary>Запускает пулю в заданную точку.
    /// <para>targetPoint - Точка, в которую полетит пуля.</para>
    /// </summary>
    public void StartBullet(Vector3 targetPoint)
    {
        target = targetPoint;
        myRig = this.GetComponent<Rigidbody>();
        this.transform.SetParent(null);
        this.transform.LookAt(target);
        this.gameObject.SetActive(true);
    }


    void Update()
    {
        if(myRig != null)
        {
            myRig.AddForce(transform.forward * bullet.speed);
        }
    }
    private void OnCollisionEnter(Collision collision) //Лучше бы сделать подписку от системы (через UniRX), но не думаю что импорт UniRX в тестовое - хорошая идея.
    {
        //----------------------------Возврат пули в пул----------------------------------
        this.gameObject.SetActive(false);
        this.transform.SetParent(ShootController.bulletSpawnPos);
        this.transform.localPosition = Vector3.zero;
        myRig.velocity = Vector3.zero;
        myRig.angularVelocity = Vector3.zero;
        //-----------------------------Взаимодействие с целью-----------------------------
        Rigidbody targetRig = collision.gameObject.GetComponent<Rigidbody>();
        if(targetRig != null)
        {
            
            Transform obj = collision.transform;
            while(obj.tag != "Enemy")
            {
                obj = obj.parent;
            }
            obj.GetComponent<Animator>().enabled = false;
            targetRig.AddForce(collision.transform.forward * (-1*bullet.force), ForceMode.Impulse);
            GameObject.Find("GameController").GetComponent<SlowMotion>().Slow();
        }


    }
}
