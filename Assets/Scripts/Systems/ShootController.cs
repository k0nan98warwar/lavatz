﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ShootController : MonoBehaviour
{
    public Animator playerAnimator;
    public static Transform bulletSpawnPos;
    public static bool shootBlocked = false; //Проверка анимации выстрела
    void Start()
    {
        bulletSpawnPos = GameObject.Find("BulletSpawnPos").transform;
    }
    /// <summary>Блокирует Update скрипта. Используйте для того, чтобы отключить управление во время анимаций.
    /// </summary>
    public void Block()
    {
        shootBlocked = true;
    }
    /// <summary>Снова активирует Update скрипта. Используйте для того, чтобы включить управление по завершению всех анимаций.
    /// </summary>
    public void unBlock()
    {
        shootBlocked = false;
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1") && !shootBlocked) //Если нажата ЛКМ и анимация выстрела ещё не запущена.
        {

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); //Рейкаст из курсора
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {

                if (hit.point.y < 5) //Чтоб не стрелять при нажатии на кнопки интерфейса (они сверху) и не ковырять графический рейкаст
                {
                    bulletSpawnPos.GetChild(0).GetComponent<Bullet>().StartBullet(hit.point);
                    playerAnimator.SetTrigger("Fire"); //Запуск анимации стрельбы на игроке
                }
            }
            else
            {
                bulletSpawnPos.GetChild(0).GetComponent<Bullet>().StartBullet(ray.GetPoint(300)); //Стрельба по воздуху
                playerAnimator.SetTrigger("Fire"); //Запуск анимации стрельбы на игроке
            }

        }
    }
}
