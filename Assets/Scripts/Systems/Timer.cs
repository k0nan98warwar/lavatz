﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour
{
    public UnityEvent eventList;
    float timer = 0;
    /// <summary>Активирует таймер и устанавливает его на заданное время.
    /// <para>time - Время до выполнения действий.</para>
    /// </summary>
    public void StartTimer(float time)
    {

        this.gameObject.SetActive(true);
        timer = time;
    }

    
    void Update()
    {
        if(timer > 0)
        {
            timer -= Time.deltaTime;
        } else
        {
            this.gameObject.SetActive(false);
            eventList.Invoke();

        }
    }
}
