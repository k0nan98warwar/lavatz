﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartGame : MonoBehaviour
{
    GameObject[] enemys;
    public void Start()
    {
        enemys = GameObject.FindGameObjectsWithTag("Enemy");
    }
    /// <summary>Возвращает всех врагов в исходное положение (Включает аниматоры).
    /// </summary>
    public void Restart()
    {
        foreach(GameObject enemy in enemys)
        {
            enemy.GetComponent<Animator>().enabled = true;
        }
    }
}
