﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimController : MonoBehaviour
{
    public ShootController shootController;
    /// <summary>Вызывается из анимации, в самом её начале.
    /// </summary>
    public void BlockInput()
    {
        shootController.Block();
    }
    /// <summary>Вызывается из анимации, в её конце
    /// </summary>
    public void unBlockInput()
    {
        shootController.unBlock();
    }
}
