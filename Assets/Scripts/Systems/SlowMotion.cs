﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif
public class SlowMotion : MonoBehaviour
{
    [HideInInspector]
    public bool startTimer = true; //Равно Use timer, при установке с редактора

    [HideInInspector]
    public Timer timer; //Показывается в Inspector-е только если стоит галочка на Use timer.
    
    /// <summary>Активирует замедление времени (на 3 секунды если включён таймер).
    /// </summary>
    public void Slow()
    {
        Time.timeScale = 0.2f;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
        if (startTimer && timer != null)
        {
            timer.StartTimer(3 * Time.timeScale); //Таймер на 3 секунды.
        }
    }
    /// <summary>Убирает эффект замедления времени.
    /// </summary>
    public void Normal()
    {
        Time.timeScale = 1f;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
    }
}
#if UNITY_EDITOR
[CustomEditor(typeof(SlowMotion))]
public class SlowMotion_Editor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        SlowMotion script = (SlowMotion)target;
        script.startTimer = EditorGUILayout.Toggle("Use timer?", script.startTimer);
        if (script.startTimer)
        {
            script.timer = EditorGUILayout.ObjectField("Timer:", script.timer, typeof(Timer), true) as Timer;
        }

    }
}
#endif