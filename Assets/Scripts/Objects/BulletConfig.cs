﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newBulletConfig", menuName = "BulletConfig", order = 43)]
public class BulletConfig : ScriptableObject
{
    public int speed;
    public int force;
}
